package br.nemo.immigrant.ontology.entity.osdef.defects.repositories;

import br.nemo.immigrant.ontology.entity.osdef.defects.models.Fault;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface FaultRepository extends PagingAndSortingRepository<Fault, Long>, ListCrudRepository<Fault, Long> {

}
