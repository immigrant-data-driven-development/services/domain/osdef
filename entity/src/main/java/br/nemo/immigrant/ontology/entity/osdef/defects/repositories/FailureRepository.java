package br.nemo.immigrant.ontology.entity.osdef.defects.repositories;

import br.nemo.immigrant.ontology.entity.osdef.defects.models.Failure;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface FailureRepository extends PagingAndSortingRepository<Failure, Long>, ListCrudRepository<Failure, Long> {

}
