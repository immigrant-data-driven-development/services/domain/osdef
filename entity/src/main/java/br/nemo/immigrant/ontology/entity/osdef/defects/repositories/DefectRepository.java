package br.nemo.immigrant.ontology.entity.osdef.defects.repositories;

import br.nemo.immigrant.ontology.entity.osdef.defects.models.Defect;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface DefectRepository extends PagingAndSortingRepository<Defect, Long>, ListCrudRepository<Defect, Long> {

}
