package br.nemo.immigrant.ontology.entity.osdef.defects.repositories;

import br.nemo.immigrant.ontology.entity.osdef.defects.models.VulnerableState;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface VulnerableStateRepository extends PagingAndSortingRepository<VulnerableState, Long>, ListCrudRepository<VulnerableState, Long> {

}
