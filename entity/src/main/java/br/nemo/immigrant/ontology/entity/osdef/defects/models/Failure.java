package br.nemo.immigrant.ontology.entity.osdef.defects.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.osdef.defects.models.CommonConceptDate;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "failure")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class Failure extends CommonConceptDate implements Serializable {




  @ManyToOne
  @JoinColumn(name = "failure_id")
  private Failure failure;

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "failure")
  @Builder.Default
  Set<Failure> failures = new HashSet<>();

  @ManyToMany
  @JoinTable(
      name = "failure_vulnerability",
      joinColumns = @JoinColumn(name = "failure_id"),
      inverseJoinColumns = @JoinColumn(name = "vulnerability_id")
  )
  @Builder.Default
  private Set<Vulnerability> vulnerabilitys = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "vulnerablestate_id")
  private VulnerableState vulnerablestate;





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        Failure elem = (Failure) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Failure {" +
         "id="+this.id+

          ", name='"+this.name+"'"+
          ", description='"+this.description+"'"+
          ", startDate='"+this.startDate+"'"+
          ", endDate='"+this.endDate+"'"+
          ", externalId='"+this.externalId+"'"+
          ", internalId='"+this.internalId+"'"+

      '}';
  }
}
