package br.nemo.immigrant.ontology.entity.osdef.defects.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.osdef.defects.models.Defect;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "fault")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class Fault extends Defect implements Serializable {




  @ManyToMany(mappedBy = "faults")
  @Builder.Default
  private Set<Failure> failures = new HashSet<>();

  @ManyToMany(mappedBy = "faults")
  @Builder.Default
  private Set<VulnerableState> vulnerablestates = new HashSet<>();





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        Fault elem = (Fault) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "Fault {" +
         "id="+this.id+



      '}';
  }
}
