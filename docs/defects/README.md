# 📕Documentation: Defects



## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **CommonConceptDate** : -
* **Failure** : Failure is defined as an Event in which a program does not perform as intended, i.e.,
an event that hurts the goals of stakeholders. 
* **VulnerableState** : denotes the situation that activates the Disposition that will be manifested in that Failure
* **Vulnerability** : -
* **ProgramVulnerability** : -
* **Defect** : A Defect is a type of Vulnerability that can exist in Programs
* **Fault** :  Defect is manifested in a Failure, we term that Defect a Fault (Runtime Defect)
