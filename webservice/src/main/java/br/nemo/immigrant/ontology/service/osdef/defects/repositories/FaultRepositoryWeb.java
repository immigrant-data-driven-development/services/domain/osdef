package br.nemo.immigrant.ontology.service.osdef.defects.repositories;

import br.nemo.immigrant.ontology.entity.osdef.defects.models.Fault;
import br.nemo.immigrant.ontology.entity.osdef.defects.repositories.FaultRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "fault", path = "fault")
public interface FaultRepositoryWeb extends FaultRepository {

}
