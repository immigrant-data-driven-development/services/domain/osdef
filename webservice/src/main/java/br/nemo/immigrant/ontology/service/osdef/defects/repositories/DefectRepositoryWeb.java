package br.nemo.immigrant.ontology.service.osdef.defects.repositories;

import br.nemo.immigrant.ontology.entity.osdef.defects.models.Defect;
import br.nemo.immigrant.ontology.entity.osdef.defects.repositories.DefectRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "defect", path = "defect")
public interface DefectRepositoryWeb extends DefectRepository {

}
