package br.nemo.immigrant.ontology.service.osdef.defects.repositories;

import br.nemo.immigrant.ontology.entity.osdef.defects.models.VulnerableState;
import br.nemo.immigrant.ontology.entity.osdef.defects.repositories.VulnerableStateRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "vulnerablestate", path = "vulnerablestate")
public interface VulnerableStateRepositoryWeb extends VulnerableStateRepository {

}
