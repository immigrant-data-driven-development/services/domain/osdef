package br.nemo.immigrant.ontology.service.osdef.defects.controllers;

import br.nemo.immigrant.ontology.entity.osdef.defects.models.VulnerableState;
import br.nemo.immigrant.ontology.entity.osdef.defects.repositories.VulnerableStateRepository;
import br.nemo.immigrant.ontology.service.osdef.defects.records.VulnerableStateInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class VulnerableStateController  {

  @Autowired
  VulnerableStateRepository repository;

  @QueryMapping
  public List<VulnerableState> findAllVulnerableStates() {
    return repository.findAll();
  }

  @QueryMapping
  public VulnerableState findByIDVulnerableState(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public VulnerableState createVulnerableState(@Argument VulnerableStateInput input) {
    VulnerableState instance = VulnerableState.builder().name(input.name()).
                                                         description(input.description()).
                                                         startDate(input.startDate()).
                                                         endDate(input.endDate()).
                                                         externalId(input.externalId()).
                                                         internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public VulnerableState updateVulnerableState(@Argument Long id, @Argument VulnerableStateInput input) {
    VulnerableState instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("VulnerableState not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setStartDate(input.startDate());
    instance.setEndDate(input.endDate());
    instance.setExternalId(input.externalId());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteVulnerableState(@Argument Long id) {
    repository.deleteById(id);
  }

}
