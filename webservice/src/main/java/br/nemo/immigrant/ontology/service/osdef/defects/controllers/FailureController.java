package br.nemo.immigrant.ontology.service.osdef.defects.controllers;

import br.nemo.immigrant.ontology.entity.osdef.defects.models.Failure;
import br.nemo.immigrant.ontology.entity.osdef.defects.repositories.FailureRepository;
import br.nemo.immigrant.ontology.service.osdef.defects.records.FailureInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class FailureController  {

  @Autowired
  FailureRepository repository;

  @QueryMapping
  public List<Failure> findAllFailures() {
    return repository.findAll();
  }

  @QueryMapping
  public Failure findByIDFailure(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Failure createFailure(@Argument FailureInput input) {
    Failure instance = Failure.builder().name(input.name()).
                                         description(input.description()).
                                         startDate(input.startDate()).
                                         endDate(input.endDate()).
                                         externalId(input.externalId()).
                                         internalId(input.internalId()).build();

    return repository.save(instance);
  }

  @MutationMapping
  public Failure updateFailure(@Argument Long id, @Argument FailureInput input) {
    Failure instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Failure not found");
    }
    instance.setName(input.name());
    instance.setDescription(input.description());
    instance.setStartDate(input.startDate());
    instance.setEndDate(input.endDate());
    instance.setExternalId(input.externalId());
    instance.setInternalId(input.internalId());
    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteFailure(@Argument Long id) {
    repository.deleteById(id);
  }

}
