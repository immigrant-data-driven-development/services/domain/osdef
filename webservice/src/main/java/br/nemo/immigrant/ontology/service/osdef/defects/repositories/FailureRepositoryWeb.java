package br.nemo.immigrant.ontology.service.osdef.defects.repositories;

import br.nemo.immigrant.ontology.entity.osdef.defects.models.Failure;
import br.nemo.immigrant.ontology.entity.osdef.defects.repositories.FailureRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "failure", path = "failure")
public interface FailureRepositoryWeb extends FailureRepository {

}
