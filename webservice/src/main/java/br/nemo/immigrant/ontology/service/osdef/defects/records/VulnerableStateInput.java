package br.nemo.immigrant.ontology.service.osdef.defects.records;
import java.time.LocalDate;
public record VulnerableStateInput( String name,String description,LocalDate startDate,LocalDate endDate,String externalId,String internalId ) {
}
