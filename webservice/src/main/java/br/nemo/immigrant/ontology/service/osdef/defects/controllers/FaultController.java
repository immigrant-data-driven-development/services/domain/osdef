package br.nemo.immigrant.ontology.service.osdef.defects.controllers;

import br.nemo.immigrant.ontology.entity.osdef.defects.models.Fault;
import br.nemo.immigrant.ontology.entity.osdef.defects.repositories.FaultRepository;
import br.nemo.immigrant.ontology.service.osdef.defects.records.FaultInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class FaultController  {

  @Autowired
  FaultRepository repository;

  @QueryMapping
  public List<Fault> findAllFaults() {
    return repository.findAll();
  }

  @QueryMapping
  public Fault findByIDFault(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Fault createFault(@Argument FaultInput input) {
    Fault instance = Fault.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public Fault updateFault(@Argument Long id, @Argument FaultInput input) {
    Fault instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Fault not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteFault(@Argument Long id) {
    repository.deleteById(id);
  }

}
