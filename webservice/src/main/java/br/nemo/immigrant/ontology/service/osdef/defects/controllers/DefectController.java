package br.nemo.immigrant.ontology.service.osdef.defects.controllers;

import br.nemo.immigrant.ontology.entity.osdef.defects.models.Defect;
import br.nemo.immigrant.ontology.entity.osdef.defects.repositories.DefectRepository;
import br.nemo.immigrant.ontology.service.osdef.defects.records.DefectInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class DefectController  {

  @Autowired
  DefectRepository repository;

  @QueryMapping
  public List<Defect> findAllDefects() {
    return repository.findAll();
  }

  @QueryMapping
  public Defect findByIDDefect(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public Defect createDefect(@Argument DefectInput input) {
    Defect instance = Defect.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public Defect updateDefect(@Argument Long id, @Argument DefectInput input) {
    Defect instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("Defect not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteDefect(@Argument Long id) {
    repository.deleteById(id);
  }

}
